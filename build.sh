#!/bin/sh

IMAGE_NAME=resume

docker build . -t ${IMAGE_NAME}

if [ $? -eq 0 ]; then
    echo "Successfully built resume - copying to this directory"
    ID=$(docker create ${IMAGE_NAME})
    docker cp ${ID}:/app/resume.pdf - > ./resume.pdf
    docker rm -v $ID
else
    echo "Failed to build docker image"
fi
