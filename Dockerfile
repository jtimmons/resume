FROM ubuntu:23.10

RUN apt -qq update && apt install -y --no-install-recommends \
  latexmk \
  texlive-fonts-extra \
  texlive-fonts-recommended \
  texlive-latex-base \
  texlive-latex-extra \
  texlive-latex-recommended \
  texlive-luatex \
  texlive-xetex \
  texlive-pictures \
  texlive-lang-french \
  lmodern \
  fonts-font-awesome

WORKDIR /app
COPY . .

RUN latexmk -cd -f -lualatex -interaction=nonstopmode -synctex=1 resume.tex
